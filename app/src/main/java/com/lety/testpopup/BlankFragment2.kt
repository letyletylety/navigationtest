package com.lety.testpopup


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.lety.testpopup.databinding.FragmentBlankFragment2Binding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class BlankFragment2 : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding = DataBindingUtil.inflate<FragmentBlankFragment2Binding>(inflater, R.layout.fragment_blank_fragment2, container, false)

        binding.bText.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_blankFragment2_to_blankFragment3)
        }

        binding.gotodButton.setOnClickListener{
            Navigation.findNavController(it).navigate(R.id.action_blankFragment2_to_blankFragment4)
        }

        return binding.root
    }


}
